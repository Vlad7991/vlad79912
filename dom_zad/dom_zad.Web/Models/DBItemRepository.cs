﻿using dom_zad.Models;
using dom_zad.Web.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dom_zad.Web.Models
{
    public class DBItemRepository : IItemRepository
    {
        private readonly MyDBCon _myDBCon;

        public DBItemRepository(MyDBCon myDBCon)
        {
            _myDBCon = myDBCon;
        }
        public void Add(Item item)
        {
            _myDBCon.Items.Add(item);
            _myDBCon.SaveChanges();
        }

        public Item Get(string id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Item> GetAll()
        {
            return _myDBCon.Items.ToList();
           
        }

        public Item Remove(string key)
        {
            throw new NotImplementedException();
        }

        public void Update(Item item)
        {
          //  _myDBCon.Items.Add(item);
            _myDBCon.SaveChanges();

        }
    }
}
