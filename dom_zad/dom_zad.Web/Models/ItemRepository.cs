﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;

namespace dom_zad.Models
{
    public class ItemRepository : IItemRepository
    {
        private static ConcurrentDictionary<string, Item> items =
            new ConcurrentDictionary<string, Item>();

        public ItemRepository()
        {
            if (items.Count==0) {
                Add(new Item { Id = Guid.NewGuid().ToString(), Name = "Vlad", Surname = "Petriuk", nralb = "w60083", plec = "M", imgUrl = "nazar.png" ,email="obito@konocha",password="12345"});
                Add(new Item { Id = Guid.NewGuid().ToString(), Name = "Vlad", Surname = "Petriuk", nralb = "w60083", plec = "M", imgUrl = "https://upload.wikimedia.org/wikipedia/commons/6/63/Gorila.JPG", email = "obito@konocha", password = "12345" });
                Add(new Item { Id = Guid.NewGuid().ToString(), Name = "Vlad", Surname = "Petriuk", nralb = "w60083", plec = "M", imgUrl = "nazar", email = "obito@konocha", password = "12345" });
                Add(new Item { Id = Guid.NewGuid().ToString(), Name = "Vlad", Surname = "Petriuk", nralb = "w60083", plec = "M", imgUrl = "nazar", email = "obito@konocha", password = "12345" }); }
        }

        public IEnumerable<Item> GetAll()
        {
            return items.Values;
        }

        public void Add(Item item)
        {
            item.Id = Guid.NewGuid().ToString();
            items[item.Id] = item;
        }

        public Item Get(string id)
        {
            items.TryGetValue(id, out Item item);
            return item;
        }

        public Item Remove(string id)
        {
            items.TryRemove(id, out Item item);
            return item;
        }

        public void Update(Item item)
        {
            items[item.Id] = item;
        }
    }
}
