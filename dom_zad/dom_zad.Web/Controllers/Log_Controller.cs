﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dom_zad.Models;
using dom_zad.ViewModels;
using dom_zad.Web.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace dom_zad.Web.Controllers
{
    [Route("api/logowanie")]
    [ApiController]
    public class Log_Controller : ControllerBase
    {
         MyDBCon _myDbContext = new MyDBCon();
        private readonly IItemRepository ItemRepository;
        private Item item;
        // POST api/<controller>
        public Log_Controller(IItemRepository itemRepository)
        {
            ItemRepository = itemRepository;
        }


        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Item> Post([FromBody] StrLogViewModel log)
        {
            if (_myDbContext.Items.Any(user => user.email.Equals(log.email)))
            {
                var user = _myDbContext.Items.Where(u => u.email.Equals(log.email)).First();

                if (log.email.Equals(user.email))
                {
                    if (log.password.Equals(user.password))
                    {
                        this.item = user;
                        return item;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }

        }


    }

}
