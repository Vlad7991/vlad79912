﻿using dom_zad.Models;
using Microsoft.EntityFrameworkCore;

using System;
using Microsoft.EntityFrameworkCore.SqlServer;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dom_zad.Web.Data
{
    public class MyDBCon : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=.\SQLEXPRESS; Database=w60083; Trusted_Connection=True;");
        }


        public DbSet<Item> Items { get; set; }
    }
}
