﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using dom_zad.Services;

namespace dom_zad.Droid
{
    class AndroidTestA : ITestA
    {
        public async Task<string> GetPlatform()
        {
            return "Android";

        }
    }
}