﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dom_zad.Models
{
    public enum MenuItemType
    {
        Browse,
        About,
        CovidTracker
    }
    public class HomeMenuItem
    {
        public MenuItemType Id { get; set; }

        public string Title { get; set; }
        
    }
}
