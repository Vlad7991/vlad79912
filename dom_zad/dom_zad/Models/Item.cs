﻿using System;

namespace dom_zad.Models
{
    public class Item
    {

        public string Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string nralb { get; set; }
        public string plec { get; set; }
        public string imgUrl { get; set; }
        public string email { get; set; }
        public string password { get; set; }

    }
}