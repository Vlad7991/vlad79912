﻿using dom_zad.Services;
using dom_zad.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace dom_zad.Views
{
    

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class strlog : ContentPage
    {
        private readonly AzureDataStore azureData = new AzureDataStore();
        private StrLogViewModel log;
        public strlog()
        {


            log = new StrLogViewModel
            {
                email = "",
                password = ""
            };
            log.Navi = this.Navigation;

            BindingContext = log;
            InitializeComponent();
        }

        private async void button_log_Clicked(object sender, EventArgs e)
        {


            if (StClasLog.user != null)
            {
                await Navigation.PushAsync(new ItemsPage());
            }


        }


        


        private async void Rejestracja_clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new NewItemPage());

        }
    }

}