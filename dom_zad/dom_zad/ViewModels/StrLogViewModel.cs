﻿using dom_zad.Models;
using dom_zad.Services;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace dom_zad.ViewModels
{
    public class StrLogViewModel: BaseViewModel
    {
        private AzureDataStore _azureAPI = new AzureDataStore();


        public string email { get; set; }
        public string password { get; set; }
        public bool success { get; set; }
        public  Item AccesUser { get; set; }
        public Xamarin.Forms.INavigation Navi;


        public StrLogViewModel()
        {


        }

        public StrLogViewModel(string email, string password)
        {
            this.email = email;
            this.password = password;
        }

        public ICommand StrLog_Command
        {
            get
            {
                return new Command(async () =>
                {
                    StrLogViewModel log = new StrLogViewModel(email, password);
                    AccesUser = await _azureAPI.Val_user(log);
                    StClasLog.user = AccesUser;

                });
            }
        }

    }
}
